# Welcome to Magictallguy's Repo: Gang Ranks

### Some random crap here trying to make you want to use it..

### Installation:
* Import and run gangranks.sql
* Upload and include my class_mtg_functions.php if you haven't already
* Edit yourgang.php and follow the instructions in yourgang_additions.php
* Continue editing yourgang.php, find the link to the Staff Room, change to this:
```PHP
echo gangHasAccess('has_rank') ? "<a href='yourgang.php?action=staff&amp;act2=idx'>Staff Room</a>" : "&nbsp;";
```

* You'll also need to add links to the Gang Staff panel to allow users to be able to manage their ranks.. This is similar to how my system was set up.. You may need to edit to fit yours
```PHP
<td><?php echo gangHasAccess('manage_ranks') ? "<a href='yourgang.php?action=staff&amp;act2=rankmanage'>Manage Ranks</a>" : "<span style='color:#666;'>Manage Ranks</span>"; ?></td>
<td><?php echo gangHasAccess('manage_ranks') ? "<a href='yourgang.php?action=staff&amp;act2=rankset'>Set Member Rank</a>" : "<span style='color:#666;'>Set Member Rank</span>"; ?></td>
```
* You'll then need to edit each and every one of the gang_staff*() functions in order to implement the actual permissions-based access.

For example, here's one of my setups..
```PHP
function gang_staff_orgcrimes() {
	global $set, $db, $my, $mtg, $gangs, $data;
	if(!gangHasAccess('organised_crimes'))
		$mtg->error("You don't have access to this");
```
Edit this to your own liking