<?php // placed here for syntax highlighter - make sure to remove it!

function gang_staff_ranks_set() {
	global $set, $db, $ir, $mtg, $gangs, $data;
	if(!gangHasAccess('manage_ranks'))
		$mtg->error("You don't have access to this");
	if(!$data['gangUSABLE'])
		$mtg->error("Your ".$set['gang_name']." needs repairing before you can do that");
	?><h3>Rank Management: Setting Member Rank</h3><?php
	if(!isset($_POST['submit'])) {
		?><form action='yourgang.php?action=staff&amp;act2=rankset' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Player</th>
					<td width='75%'><select name='user1'><?php
						$selectMembers = $db->query("SELECT `userid`, `username` FROM `users` WHERE `gang` = ".$data['gangID']." ORDER BY `username` ASC");
						while($row = $db->fetch_row($selectMembers))
							printf("<option value='%u'>%s</option>", $row['userid'], $mtg->format($row['username']));
					?></select></td>
				</tr>
				<tr>
					<th><u>OR</u> Player ID</th>
					<td><input type='number' name='user2' /></td>
				</tr>
				<tr>
					<th>Rank</th>
					<td><select name='rank'>
						<option value='0'>None</option><?php
						$selectRanks = $db->query("SELECT `id`, `name` FROM `gang_ranks` WHERE `gang` = ".$data['gangID']." ORDER BY `id` ASC");
						while($row = $db->fetch_row($selectRanks))
							printf("<option value='%u'>%s</option>", $row['id'], $mtg->format($row['name']));
					?></select></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='submit' value='Change Rank' /></td>
				</tr>
			</table>
		</form><?php
	} else {
		$_POST['user1'] = isset($_POST['user1']) && ctype_digit($_POST['user1']) ? $_POST['user1'] : null;
		$_POST['user2'] = isset($_POST['user2']) && ctype_digit($_POST['user2']) ? $_POST['user2'] : null;
		if(empty($_POST['user1']) && empty($_POST['user2']))
			$mtg->error("You didn't select a valid player");
		if(!empty($_POST['user1']) && !empty($_POST['user2']))
			$mtg->error("Select one option only");
		$_POST['user'] = empty($_POST['user2']) ? $_POST['user1'] : $_POST['user2'];
		if($data['gangPRESIDENT'] == $_POST['user'])
			$mtg->error("The President's rank can't be changed");
		$select = $db->query("SELECT `gang` FROM `users` WHERE `userid` = ".$_POST['user']);
		if(!$db->num_rows($select))
			$mtg->error("That player doesn't exist");
		if($db->fetch_single($select) != $data['gangID'])
			$mtg->error($mtg->username($_POST['user'])." isn't in your ".ucfirst($set['gang_name']));
		$_POST['rank'] = isset($_POST['rank']) && ctype_digit($_POST['rank']) ? $_POST['rank'] : 0;
		$rank = 'none';
		if($_POST['rank']) {
			$selectRank = $db->query("SELECT `name` FROM `gang_ranks` WHERE `id` = ".$_POST['rank']." AND `gang` = ".$data['gangID']);
			if(!$db->num_rows($selectRank))
				$mtg->error("Either that rank doesn't exist or it's not yours");
			$rank = $mtg->format($db->fetch_single($selectRank));
		}
		$db->query("UPDATE `users` SET `gang_rank` = ".$_POST['rank']." WHERE `userid` = ".$_POST['user']);
		$gangs->event_add($data['gangID'], $ir['userid'], "Set ".$mtg->username($_POST['user'])."'s rank to ".$rank);
		$mtg->success("You've set ".$mtg->username($_POST['user'])."'s ".ucfirst($set['gang_name'])." rank to ".$rank);
	}
}
function gang_staff_ranks_manage() {
	global $set, $db, $ir, $mtg, $gangs, $data;
	if(!gangHasAccess('manage_ranks'))
		$mtg->error("You don't have access to this");
	if(!$data['gangUSABLE'])
		$mtg->error("Your ".$set['gang_name']." needs repairing before you can do that");
	?><h3>Rank Management - <a href='yourgang.php?action=staff&amp;act2=rankadd'>Add Rank</a></h3><?php
	$selectRanks = $db->query("SELECT `id`, `name` FROM `gang_ranks` WHERE `gang` = ".$data['gangID']." ORDER BY `id` ASC");
	?><table class='table' width='75%'>
		<tr>
			<th width='60%'>Rank</th>
			<th width='40%'>Actions</th>
		</tr><?php
		if(!$db->num_rows($selectRanks))
			echo "<tr><td colspan='2' class='center'>Your ".ucfirst($set['gang_name'])." doesn't currently have any ranks</td></tr>";
		else
			while($row = $db->fetch_row($selectRanks)) {
				?><tr>
					<td><?php echo $mtg->format($row['name']); ?></td>
					<td>
						<a href='yourgang.php?action=staff&amp;act2=rankedit&amp;ID=<?php echo $row['id']; ?>'><img src='img/silk/pencil.png' title='Edit' alt='Edit' /></a>
						<a href='yourgang.php?action=staff&amp;act2=rankdel&amp;ID=<?php echo $row['id']; ?>'><img src='img/silk/delete.png' title='Delete' alt='Delete' /></a></td>
				</tr><?php
			}
	?></table><?php
}
function gang_staff_ranks_add() {
	global $set, $db, $ir, $mtg, $gangs, $data;
	if(!gangHasAccess('manage_ranks'))
		$mtg->error("You don't have access to this");
	if(!$data['gangUSABLE'])
		$mtg->error("Your ".$set['gang_name']." needs repairing before you can do that");
	?><h3>Rank Management: Add</h3><?php
	$fields = array();
	$select = $db->query("SHOW COLUMNS FROM `gang_ranks` WHERE `Type` = 'enum(\'Yes\',\'No\')'");
	while($row = $db->fetch_row($select))
		array_push($fields, $row['Field']);
	if(!isset($_POST['submit'])) {
		?><form action='yourgang.php?action=staff&amp;act2=rankadd' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Name</th>
					<td width='75%'><input type='text' name='name' /></td>
				</tr><?php
				foreach($fields as $col) {
					?><tr>
						<th><?php echo ucwords(str_replace('_', ' ', $col)); ?></th>
						<td><select name='<?php echo $col; ?>' style='width:50%;'>
							<option value='No' selected='selected'>Disabled</option>
							<option value='Yes'>Enabled</option>
						</select></td>
					</tr><?php
				}
				?><tr>
					<td colspan='2' class='center'><input type='submit' name='submit' value='Add Rank' /></td>
				</tr>
			</table>
		</form><?php
	} else {
		$_POST['name'] = isset($_POST['name']) ? $db->escape($_POST['name']) : null;
		if(empty($_POST['name']))
			$mtg->error("You didn't select a valid name");
		$selectRank = $db->query("SELECT `id` FROM `gang_ranks` WHERE `name` = '".$_POST['name']."' AND `gang` = ".$data['gangID']);
		if($db->num_rows($selectRank))
			$mtg->error("A rank with that name already exists");
		$db->query("INSERT INTO `gang_ranks` (`gang`, `name`) VALUES (".$data['gangID'].", '".$_POST['name']."')");
		$new = $db->insert_id();
		$name = $_POST['name'];
		unset($_POST['submit'], $_POST['name']);
		echo "POSTDATA<br /><pre>",print_r($_POST, true),"</pre>";
		foreach($_POST as $what => $value) {
			if(!in_array($what, $fields))
				$mtg->error("Invalid input");
			if(!in_array($value, array('Yes', 'No')))
				$mtg->error("Invalid selection made: ".$what);
			$db->query("UPDATE `gang_ranks` SET `".$what."` = '".$db->escape($value)."' WHERE `id` = ".$new);
		}
		$gangs->event_add($data['gangID'], $ir['userid'], "Added a new rank: ".$mtg->format($name));
		$mtg->success("You've created a new ".ucfirst($set['gang_name'])." rank: ".$mtg->format($name));
	}
}
function gang_staff_ranks_edit() {
	global $set, $db, $ir, $mtg, $gangs, $data;
	if(!gangHasAccess('manage_ranks'))
		$mtg->error("You don't have access to this");
	if(!$data['gangUSABLE'])
		$mtg->error("Your ".$set['gang_name']." needs repairing before you can do that");
	?><h3>Rank Management: Edit</h3><?php
	$fields = array();
	$select = $db->query("SHOW COLUMNS FROM `gang_ranks` WHERE `Type` = 'enum(\'Yes\',\'No\')'");
	while($row = $db->fetch_row($select))
		array_push($fields, $row['Field']);
	if(empty($_GET['ID']))
		$mtg->error("You didn't specify a valid rank");
	$select = $db->query("SELECT `name` FROM `gang_ranks` WHERE `id` = ".$_GET['ID']." AND `gang` = ".$data['gangID']);
	if(!$db->num_rows($select))
		$mtg->error("Either that rank doesn't exist or it's not yours");
	$row = $db->fetch_row($select);
	$_GET['step'] = isset($_GET['step']) && ctype_digit($_GET['step']) ? $_GET['step'] : null;
	if(!isset($_POST['submit'])) {
		?><form action='yourgang.php?action=staff&amp;act2=rankedit&amp;ID=<?php echo $_GET['ID']; ?>' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='60%'>Name</th>
					<td width='40%'><input type='text' name='name' value='<?php echo $mtg->format($row['name']); ?>' /></td>
				</tr><?php
				foreach($fields as $col) {
					$selectSingle = $db->query("SELECT `".$col."` FROM `gang_ranks` WHERE `id` = ".$_GET['ID']);
					$single = $db->fetch_single($selectSingle);
					?><tr>
						<th><?php echo ucwords(str_replace('_', ' ', $col)); ?></th>
						<td><select name='<?php echo $col; ?>' style='width:50%;'>
							<option value='No' style='color:red;'<?php echo $single == 'No' ? " selected='selected'" : ''; ?>>Disabled</option>
							<option value='Yes' style='color:green;'<?php echo $single == 'Yes' ? " selected='selected'" : ''; ?>>Enabled</option>
						</select></td>
					</tr><?php
				}
				?><tr>
					<td colspan='2' class='center'><input type='submit' name='submit' value='Edit Rank' /></td>
				</tr>
			</table>
		</form><?php
	} else {
		unset($_POST['submit']);
		$_POST['name'] = isset($_POST['name']) ? $db->escape($_POST['name']) : null;
		if(empty($_POST['name']))
			$mtg->error("You didn't select a valid name");
		$selectRank = $db->query("SELECT `id` FROM `gang_ranks` WHERE `gang` = ".$data['gangID']." AND `name` = '".$_POST['name']."' AND `id` <> ".$_GET['ID']);
		if($db->num_rows($selectRank))
			$mtg->error("A rank with that name already exists");
		$db->query("REPLACE INTO `gang_ranks` (`id`, `gang`) VALUES (".$_GET['ID'].", ".$data['gangID'].")");
		$new = $db->insert_id();
		foreach($_POST as $what => $value)
			$db->query("UPDATE `gang_ranks` SET `".$what."` = '".$db->escape($value)."' WHERE `id` = ".$new);
		$gangs->event_add($data['gangID'], $ir['userid'], "Edited the rank: ".$mtg->format($_POST['name']));
		$mtg->success("You've edited the ".ucfirst($set['gang_name'])." rank rank: ".$mtg->format($_POST['name']));
	}
}
function gang_staff_ranks_delete() {
	global $set, $db, $ir, $mtg, $gangs, $data;
	if(!gangHasAccess('manage_ranks'))
		$mtg->error("You don't have access to this");
	if(!$data['gangUSABLE'])
		$mtg->error("Your ".$set['gang_name']." needs repairing before you can do that");
	?><h3>Rank Management: Delete</h3><?php
	if(empty($_GET['ID']))
		$mtg->error("You didn't specify a valid rank");
	$select = $db->query("SELECT `name` FROM `gang_ranks` WHERE `id` = ".$_GET['ID']." AND `gang` = ".$data['gangID']);
	if(!$db->num_rows($select))
		$mtg->error("Either that rank doesn't exist or it's not yours");
	$rank = $mtg->format($db->fetch_single($select));
	if(!isset($_GET['ans'])) {
		?>Are you sure you want to delete <?php echo $rank; ?>?<br />
		<a href='yourgang.php?action=staff&amp;act2=rankdel&amp;ID=<?php echo $_GET['ID']; ?>&amp;ans=yes'>Yes</a><?php
	} else {
		$db->query("DELETE FROM `gang_ranks` WHERE `id` = ".$_GET['ID']);
		$db->query("UPDATE `users` SET `gang_rank` = 0 WHERE `gang_rank` = ".$_GET['ID']);
		$gangs->event_add($data['gangID'], $ir['userid'], "Deleted the rank: ".$rank);
		$mtg->success("You've deleted the ".ucfirst($set['gang_name'])." rank: ".$rank);
	}
}
function gangHasAccess($what, $id = 0) {
	global $set, $db, $ir, $mtg, $gangs, $data;
	if($data['gangPRESIDENT'] == $ir['userid']) // || $mtg->hasAccess('override_gangs')) // Removed until my Staff: Ranks has been installed
		return true;
	if(!$id)
		$id = $ir['userid'];
	$selectUser = $db->query("SELECT `gang_rank` FROM `users` WHERE `userid` = ".$id);
	$rank = $db->fetch_single($selectUser);
	if(!$rank)
		return false;
	if($what == 'has_rank')
		return $rank ? true : false;
	$selectRank = $db->query("SELECT `".$what."` FROM `gang_ranks` WHERE `id` = ".$rank);
	if(!$db->num_rows($selectRank))
		return false;
	$perm = $db->fetch_row($selectRank);
	if($perm[$what] != 'Yes')
		return false;
	return true;
}