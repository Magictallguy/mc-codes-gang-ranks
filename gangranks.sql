-- Create gang_ranks table
CREATE TABLE `gang_ranks` (
   `id` int(11) not null auto_increment,
   `name` varchar(255) not null,
   `gang` int(11) not null default '0',
   `order` int(11) not null default '1',
   `vault_management` enum('Yes','No') not null default 'No',
   `application_management` enum('Yes','No') not null default 'No',
   `change_banner` enum('Yes','No') not null default 'No',
   `upgrade_faction` enum('Yes','No') not null default 'No',
   `organised_crimes` enum('Yes','No') not null default 'No',
   `mass_payment` enum('Yes','No') not null default 'No',
   `recall_items_to_armoury` enum('Yes','No') not null default 'No',
   `declare_war` enum('Yes','No') not null default 'No',
   `faction_announcement` enum('Yes','No') not null default 'No',
   `change_president` enum('Yes','No') not null default 'No',
   `faction_recruit` enum('Yes','No') not null default 'No',
   `surrender` enum('Yes','No') not null default 'No',
   `view_or_accept_surrenders` enum('Yes','No') not null default 'No',
   `mass_mail_faction` enum('Yes','No') not null default 'No',
   `faction_name` enum('Yes','No') not null default 'No',
   `faction_description` enum('Yes','No') not null default 'No',
   `faction_tag` enum('Yes','No') not null default 'No',
   `armoury_settings` enum('Yes','No') not null default 'No',
   `manage_armoury` enum('Yes','No') not null default 'No',
   `warring_option` enum('Yes','No') not null default 'No',
   `open_or_close_applications` enum('Yes','No') not null default 'No',
   `disband_faction` enum('Yes','No') not null default 'No',
   `manage_ranks` enum('Yes','No') not null default 'No',
   `manage_perks` enum('Yes','No') not null default 'No',
   `kick_members` enum('Yes','No') not null default 'No',
   `manage_alliances` enum('Yes','No') not null default 'No',
   `repair_faction` enum('Yes','No') not null default 'No',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Add rank to users
ALTER TABLE `users` ADD `gang_rank` INT( 11 ) NOT NULL DEFAULT 0 AFTER `gang`;